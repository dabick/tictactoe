package tictactoe

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/nu7hatch/gouuid"
	"gitlab.com/dabick/go-util/log"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

type game struct {
	Squares    []uint8
	TurnId     uint64
	PlayerX    uint64
	PlayerO    uint64
	GameId     string
	Winner     uint64
	gameType   int
	difficulty int
}

type info struct {
	GameId  string
	PlayerO uint64
	PlayerX uint64
}

const BLANK = 0
const O_INT = 2
const X_INT = 1

const (
	COMPUTER = iota
	HOTSEAT
	NETWORK_VS
	INVALID
)

const (
	INVALID_DIFFICULTY = iota
	NOVICE
	INTERMEDIATE
	ADVANCED
	EXPERT
)

const (
	UPPER_LEFT = iota
	UPPER_CENTER
	UPPER_RIGHT
	LEFT
	CENTER
	RIGHT
	LOWER_LEFT
	LOWER_CENTER
	LOWER_RIGHT
)

var winningCombos = [][]uint8{
	{UPPER_LEFT, UPPER_CENTER, UPPER_RIGHT},
	{LEFT, CENTER, RIGHT},
	{LOWER_LEFT, LOWER_CENTER, LOWER_RIGHT},
	{UPPER_LEFT, LEFT, LOWER_LEFT},
	{UPPER_CENTER, CENTER, LOWER_CENTER},
	{UPPER_RIGHT, RIGHT, LOWER_RIGHT},
	{UPPER_LEFT, CENTER, LOWER_RIGHT},
	{UPPER_RIGHT, CENTER, LOWER_LEFT},
}

//Map of Game ID to Game
var gameMap = make(map[string]game)

var waitingGames = make(chan string, 100) //Stores the gameId

var nextPlayerId uint64 = 1000

const COMPUTER_ID = 100
const TIE = 1

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

func SetupTictactoeServer(router *mux.Router) {
	router.HandleFunc("/tictactoe/register", newRegistration)
	router.HandleFunc("/tictactoe/forfeit", forfeit)
	router.HandleFunc("/tictactoe/turn", turnReceived)
	router.HandleFunc("/tictactoe/computer", getComputerTurn)
}

func generateGameId() string {
	u4, err := uuid.NewV4()
	if err != nil {
		log.Println("error:", err.Error())
		return ""
	}
	uuid := u4.String()
	log.Println("Generating game ID: " + uuid)
	return uuid
}

func forfeit(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		w.WriteHeader(400)
		results := results{Error: "Game ID and Player ID is required."}
		jsonResults, err := json.Marshal(results)
		if err != nil {
			handleError(w, err.Error())
			return
		}
		w.Write(jsonResults)
		return
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		handleError(w, err.Error())
		return
	}

	defer r.Body.Close()

	var forfeiter turn
	json.Unmarshal(body, &forfeiter)

	delete(gameMap, forfeiter.GameId)
}

func newRegistration(w http.ResponseWriter, r *http.Request) {
	select {
	case gameId := <-waitingGames:
		gameToPlay := gameMap[gameId]
		if gameToPlay.gameType == COMPUTER && gameToPlay.PlayerX != COMPUTER_ID {
			gameToPlay.PlayerO = COMPUTER_ID
		} else {
			gameToPlay.PlayerO = nextPlayerId
			nextPlayerId++
		}
		gameMap[gameId] = gameToPlay
		jsonResults, err := json.Marshal(info{
			GameId:  gameToPlay.GameId,
			PlayerO: gameToPlay.PlayerO,
			PlayerX: gameToPlay.PlayerX,
		})

		if err != nil {
			handleError(w, err.Error())
		} else {
			w.Write(jsonResults)
			return
		}
	default:
		gameType := getGameType(w, r)
		if gameType == INVALID {
			return
		}
		isX := isPlayerX(r)

		newGame := newGame()
		newGame.GameId = generateGameId()

		if gameType == COMPUTER && !isX {
			newGame.PlayerX = COMPUTER_ID
		} else {
			newGame.PlayerX = nextPlayerId
			nextPlayerId++
		}

		if gameType == COMPUTER {
			difficulty, err := getGameDifficulty(r)
			if err != nil {
				w.WriteHeader(400)
				w.Write([]byte("Invalid difficulty"))
				log.Println(err.Error())
				return
			}
			newGame.difficulty = difficulty
		}

		newGame.TurnId = newGame.PlayerX
		newGame.gameType = gameType

		gameMap[newGame.GameId] = newGame
		waitingGames <- newGame.GameId
		newRegistration(w, r)
	}
}

func getGameType(w http.ResponseWriter, r *http.Request) int {
	gameType := r.FormValue("gameType")
	switch gameType {
	case "hotseat":
		return HOTSEAT
	case "computer":
		return COMPUTER
	}
	msg := "Invalid Gametype " + gameType
	log.Println(msg)
	w.WriteHeader(400)
	w.Write([]byte(msg))
	return INVALID
}

func isPlayerX(r *http.Request) bool {
	if r.FormValue("side") == "x" {
		return true
	}
	return false
}

func getGameDifficulty(r *http.Request) (int, error) {
	if difficulty, err := strconv.Atoi(r.FormValue("difficulty")); err == nil {
		switch difficulty {
		case NOVICE:
			return NOVICE, nil
		case INTERMEDIATE:
			return INTERMEDIATE, nil
		case ADVANCED:
			return ADVANCED, nil
		case EXPERT:
			return EXPERT, nil
		}
	}
	return -1, errors.New("Invalid difficulty")
}

func turnReceived(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		return
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		handleError(w, err.Error())
		return
	}

	defer r.Body.Close()

	var currentTurn turn
	err = json.Unmarshal(body, &currentTurn)

	if err != nil {
		handleError(w, err.Error())
		return
	}

	log.Println(currentTurn)

	currentGame, ok := gameMap[currentTurn.GameId]

	var result *results = nil
	if ok == false {
		result = &results{Error: "Game does not exist"}
	} else if currentTurn.PlayerId == COMPUTER_ID {
		result = &results{Error: "It is the computer's turn!"}
	}
	handleTurnResults(result, &currentGame, &currentTurn, w)
}

func handleError(w http.ResponseWriter, err string) {
	log.Println(err)
	http.Error(w, err, http.StatusInternalServerError)
}

func handleTurnResults(result *results, currentGame *game, currentTurn *turn, w http.ResponseWriter) {
	if result == nil {
		log.Println(*currentTurn)

		result = currentGame.handleTurn(*currentTurn)
		if result.Winner > 0 {
			delete(gameMap, currentTurn.GameId)
		} else {
			gameMap[currentTurn.GameId] = *currentGame
		}
	}

	jsonResults, err := json.Marshal(result)

	if err != nil {
		handleError(w, err.Error())
		return
	}

	w.Write(jsonResults)
}

func newGame() game {
	return game{
		[]uint8{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK},
		0,
		0,
		0,
		"",
		0,
		INVALID,
		INVALID_DIFFICULTY,
	}
}

func hasWon(squares []uint8) bool {
	for _, combo := range winningCombos {
		if squares[combo[0]] != BLANK &&
			squares[combo[0]] == squares[combo[1]] &&
			squares[combo[1]] == squares[combo[2]] {
			return true
		}
	}
	return false
}

//Assuming this is only called when hasWon is false
func hasTied(currentGame *game) bool {
	for _, value := range currentGame.Squares {
		if value == BLANK {
			return false
		}
	}
	return true
}

func updateGame(gameToUpdate *game) {
	if gameToUpdate.Winner != 0 {
		gameToUpdate.TurnId = 0
	} else if gameToUpdate.TurnId == gameToUpdate.PlayerX {
		gameToUpdate.TurnId = gameToUpdate.PlayerO
	} else {
		gameToUpdate.TurnId = gameToUpdate.PlayerX
	}
}

/**
 * TODO Implement this method.
 * This will update the database with the results of the game.
 */
func updateStats(gameToUpdate *game) {
	if gameToUpdate.gameType == HOTSEAT {
		return
	}
	return
}

func getComputerTurn(w http.ResponseWriter, r *http.Request) {
	currentGame, err := getGameFromQueryParam(r)
	var result *results = nil
	var computerTurn *turn = nil

	if err != nil {
		result = &results{Error: err.Error()}
	} else if currentGame.TurnId != COMPUTER_ID ||
		(currentGame.PlayerX != COMPUTER_ID && currentGame.PlayerO != COMPUTER_ID) {
		result = &results{Error: "Not the computer's turn."}
	} else if currentGame.Winner != 0 {
		result = &results{Error: "Game already finished."}
	} else {
		computerTurn = getTurnForComputer(currentGame)
	}

	handleTurnResults(result, currentGame, computerTurn, w)
}

func getGameFromQueryParam(r *http.Request) (*game, error) {
	if currentGame, ok := gameMap[r.FormValue("gameId")]; ok {
		return &currentGame, nil
	}
	return nil, errors.New("Game does not exist")
}
