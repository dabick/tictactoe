package tictactoe

type turn struct {
	PlayerId uint64 `json:"playerId"`
	GameId   string `json:"gameId"`
	Index    uint8  `json:"index"`
}

type results struct {
	Index  uint8  `json:"index"`
	Value  uint8  `json:"value"`
	Turn   uint64 `json:"turn"`
	Winner uint64 `json:"winner"`
	Error  string `json:"err"`
}

func (currentGame *game) handleTurn(currentTurn turn) *results {
	if currentGame.TurnId != currentTurn.PlayerId {
		return &results{Error: "Not your turn."}
	} else if currentGame.Winner != 0 {
		return &results{Error: "Sorry, game is over."}
	}

	index := currentTurn.Index

	if currentGame.Squares[index] != BLANK {
		return &results{Error: "Square already occupied."}
	}

	if currentTurn.PlayerId == currentGame.PlayerX {
		currentGame.Squares[index] = X_INT
	} else {
		currentGame.Squares[index] = O_INT
	}

	if hasWon(currentGame.Squares) {
		currentGame.Winner = currentGame.TurnId
		updateStats(currentGame)
	} else if hasTied(currentGame) {
		currentGame.Winner = TIE
		updateStats(currentGame)
	}

	updateGame(currentGame)

	return &results{
		Index:  index,
		Value:  currentGame.Squares[index],
		Turn:   currentGame.TurnId,
		Winner: currentGame.Winner,
		Error:  "",
	}
}
