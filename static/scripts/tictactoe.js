function TicGame() {
    this.playerX = 0;
    this.playerO = 0;
    this.turn = 0;
    this.squares = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.gameId = "";
}

function Turn(playerId, gameId, index) {
    this.playerId = playerId;
    this.gameId = gameId;
    this.index = index;
}

function Results() {
    this.index = 0;
    this.value = 0;
    this.turn = 0;
    this.winner = 0;
    this.err = "";
}

const BLANK = 0;
const XINT = 1;
const YINT = 2;

const NO_WINNER = 0;
const COMPUTER_ID = 100;
const TIE = 1;

const COMPUTER_GAME = 'computer';

const NOVICE = 1;
const INTERMEDIATE = 2;
const ADVANCED = 3;
const EXPERT = 4;

var ticGame = new TicGame();

var difficulty = 0;

function init() {
    drawBoard();
}

function closeDialog(dialog) {
    hide(null, dialog);
}

function newComputerGame(diff) {
    difficulty = diff;
    closeDialog('difficultyPopup');
    show('overlay', 'sidePopup');
}

function newGame(gameType, side) {
    hide('overlay', 'popup');
    hide('overlay', 'sidePopup');

    if (ticGame.turn > 0) {
        if (!forfeit(" Forfeit and start a new game?")) {
            return;
        }
    }

    var url = '/tictactoe/register?gameType=';
    if (COMPUTER_GAME === gameType) {
        url += COMPUTER_GAME + '&difficulty=' + difficulty + '&side=' + side;
    } else if ('hotseat' === gameType) {
        url += 'hotseat';
    } else {
        throw new Error('invalid gametype ' + gameType);
    }

    ajaxRequest(url, "POST", registered, registered, null);
}

function errorFunction(status, message) {
    console.log(status, message);
}

function forfeit(message) {
    if (!ticGame.gameId || ticGame.turn == 0) {
        displayMessage("No game currently started.");
        return false;
    }
    if (!message) {
        message = "";
    }
    var newGame = confirm("This will forfeit your current game." + message);
    if (!newGame) {
        return false;
    }
    var turn = new Turn(ticGame.turn, ticGame.gameId, -1);
    ajaxRequest("/tictactoe/forfeit", "POST", null, null, turn);
    displayMessage("Game forfeited.");

    ticGame = new TicGame();
    drawBoard();
    return true;
}

function registered(registration, error) {
    if (registration === 409) {
        displayMessage("Already registered");
        return;
    } else if (registration === 0) {
        displayMessage("Error contacting server.");
        return;
    } else if (registration >= 300) {
        displayMessage(error)
        console.log(registration)
    }

    ticGame = new TicGame();
    ticGame.gameId = registration.GameId;
    ticGame.playerX = registration.PlayerX;
    ticGame.playerO = registration.PlayerO;
    ticGame.turn = registration.PlayerX;

    drawBoard();

    displayMessage('Playing TicTacToe!');
}

function displayMessage(message, append) {
    var messageDiv = document.getElementById("message");

    if (append) {
        messageDiv.innerHTML = messageDiv.innerHTML + message;
    } else {
        messageDiv.innerHTML = message;
    }
}

function handleClick(event) {
    if (ticGame.gameId == "") {
        displayMessage('No game in progress.');
        show('overlay', 'popup');
        return;
    }

    if (ticGame.turn === COMPUTER_ID) {
        displayMessage('It is the computer\'s turn');
        return;
    }

    var turn = new Turn(
        ticGame.turn,
        ticGame.gameId,
        getIndexFromClick(event)
    );

    ajaxRequest("/tictactoe/turn", "POST", turnResults, turnResults, turn);
}

function getIndexFromClick(event) {
    var canvas = document.getElementById("tictactoe");
    var bb = canvas.getBoundingClientRect();

    var clickX = parseInt((event.clientX - bb.left) * (canvas.width / bb.width));
    var clickY = parseInt((event.clientY - bb.top) * (canvas.height / bb.height));
    var squareSize = parseInt(canvas.width / 3);

    var column = parseInt(clickX / squareSize);
    var row = parseInt(clickY / squareSize);
    var index = column + 3 * row;
    return index;
}

function turnResults(results) {
    if (results.err && results.err.length > 0) {
        displayMessage(results.err);
        return;
    }

    var index = results.index;
    ticGame.squares[index] = results.value;
    ticGame.turn = results.turn;

    drawBoard();

    handleWinner(results);
}

function handleWinner(results) {
    var message = "";
    switch (results.winner) {
        case NO_WINNER:
            return;
        case TIE:
            message = "Game was a Tie!"
            break;
        case ticGame.playerX:
            message = "X wins!";
            break;
        case ticGame.playerO:
            message = "O wins!";
            break;
        default:
            console.log('Invalid Winner ' + results.winner);
            return;
    }

    displayMessage(message);
    ticGame = new TicGame();
}

function drawBoard() {
    var canvas = document.getElementById("tictactoe");
    var ticContext = canvas.getContext("2d");

    canvas.height = canvas.width;
    canvas.width = canvas.width;

    var squareSize = canvas.width / 3;

    ticContext.lineWidth = 2;
    ticContext.moveTo(squareSize, 0);
    ticContext.lineTo(squareSize, squareSize * 3);
    ticContext.moveTo(squareSize * 2, 0);
    ticContext.lineTo(squareSize * 2, squareSize * 3);
    ticContext.moveTo(0, squareSize);
    ticContext.lineTo(squareSize * 3, squareSize);
    ticContext.moveTo(0, squareSize * 2);
    ticContext.lineTo(squareSize * 3, squareSize * 2);
    ticContext.stroke();

    var boundBox = canvas.getBoundingClientRect();
    var canvasX = boundBox.left;
    var canvasY = boundBox.top;
    var sqHalf = squareSize / 2;

    var x, y;

    var squares = ticGame.squares;

    ticContext.textAlign = "center";
    ticContext.textBaseline = "middle";
    ticContext.font = squareSize - 10 + "px sans-serif";

    for (var i = 0; i < 7; i += 3) {
        for (var j = 0; j < 3; j++) {
            var index = i + j;
            if (squares[index] === BLANK) { continue; }
            var symbol = XINT === squares[index] ? "X" : "O";
            x = j * squareSize + sqHalf;
            y = i / 3 * squareSize + squareSize * 0.6;
            ticContext.fillText(symbol, x, y, squareSize);
        }
    }

    if (ticGame.gameId && ticGame.turn === COMPUTER_ID) {
        var url = 'tictactoe/computer?gameId=' + ticGame.gameId;
        ajaxRequest(url, "GET", turnResults, turnResults, null);
    }
}