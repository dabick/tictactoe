package tictactoe

var edges = []uint8{UPPER_LEFT, UPPER_CENTER, UPPER_RIGHT, RIGHT, LOWER_RIGHT, LOWER_CENTER, LOWER_LEFT, LEFT}

func getTurnForComputer(currentGame *game) *turn {
	emptySquares := make([]int, 0)
	for index, square := range currentGame.Squares {
		if square == BLANK {
			emptySquares = append(emptySquares, int(index))
		}
	}

	move := maybeGetIntermediate(currentGame, emptySquares)

	if currentGame.difficulty == ADVANCED && move == -1 {
		move = getAdvanced(currentGame, emptySquares)
	}
	if currentGame.difficulty == EXPERT && move == -1 {
		move = getExpert(currentGame, emptySquares)
	}

	if move == -1 {
		move = random.Intn(len(emptySquares)) //[0,n)
	}

	return &turn{
		PlayerId: COMPUTER_ID,
		GameId:   currentGame.GameId,
		Index:    uint8(emptySquares[move]),
	}
}

func maybeGetIntermediate(currentGame *game, emptySquares []int) int {
	var computerToken uint8
	var playerToken uint8
	if currentGame.PlayerX == COMPUTER_ID {
		computerToken = X_INT
		playerToken = O_INT
	} else {
		computerToken = O_INT
		playerToken = X_INT
	}

	move := -1

	if currentGame.difficulty >= INTERMEDIATE {
		move = getWinningMove(currentGame, emptySquares, computerToken)
		if move == -1 {
			move = getWinningMove(currentGame, emptySquares, playerToken)
		}
	}
	return move
}

func getWinningMove(currentGame *game, emptySquares []int, token uint8) int {
	for index, gameIndex := range emptySquares {
		squares := copyGameSquares(currentGame)
		squares[gameIndex] = token
		if hasWon(squares) {
			return index
		}
	}
	return -1
}

func copyGameSquares(currentGame *game) []uint8 {
	copySquares := make([]uint8, len(currentGame.Squares))
	copy(copySquares, currentGame.Squares)
	return copySquares
}

func getAdvanced(currentGame *game, emptySquares []int) int {
	if getTurn(emptySquares) == 1 {
		firstChoices := []int{UPPER_LEFT, UPPER_RIGHT, LOWER_LEFT, LOWER_RIGHT}

		if ruthless := random.Intn(2); ruthless != 0 {
			//Not ruthless AI, give > 50% chance of choosing middle
			firstChoices = append(firstChoices, CENTER, CENTER, CENTER, CENTER, CENTER)
		}

		choice := random.Intn(len(firstChoices))
		return firstChoices[choice]
	} else if getTurn(emptySquares) == 2 {
		return getPerfectSecondMove(currentGame)
	}
	return -1
}

func getPerfectSecondMove(currentGame *game) int {
	if currentGame.Squares[CENTER] != BLANK {
		return UPPER_LEFT
	} else {
		return CENTER
	}
}

func getTurn(emptySquares []int) int {
	return 9 - len(emptySquares) + 1
}

func getExpert(currentGame *game, emptySquares []int) int {
	choice := getExpertSquare(currentGame, getTurn(emptySquares))
	for index, value := range emptySquares {
		if value == choice {
			return index
		}
	}
	return -1
}

func getExpertSquare(currentGame *game, turn int) int {
	// For optimal moves that this is mostly based on,
	// see https://en.wikipedia.org/wiki/Tic-tac-toe
	switch turn {
	case 1:
		return UPPER_LEFT
	case 2:
		return getPerfectSecondMove(currentGame)
	case 3:
		return getMoveThree(currentGame.Squares)
	case 4:
		return getMoveFour(currentGame.Squares)
	case 5:
		return getMoveFive(currentGame.Squares)
	case 6:
		return getPossibleWin(currentGame.Squares, O_INT)
	}
	//Will never happen
	return -1
}

func getMoveThree(squares []uint8) int {
	if squares[UPPER_CENTER] == O_INT || squares[UPPER_RIGHT] == O_INT {
		return LEFT
	} else if squares[LEFT] == O_INT || squares[CENTER] == O_INT || squares[LOWER_LEFT] == O_INT {
		return UPPER_CENTER
	} else if squares[LOWER_CENTER] == O_INT || squares[LOWER_RIGHT] == O_INT {
		return UPPER_RIGHT
	}
	return CENTER
}

func getMoveFour(squares []uint8) int {
	if squares[CENTER] == O_INT && isCorner(squares, X_INT) {
		edge := getContinuousEdge(squares, X_INT)
		return getBestEdge(squares, edge)
	} else {
		return getOpenCorner(squares)
	}
}

func isCorner(squares []uint8, token uint8) bool {
	return squares[UPPER_LEFT] == token || squares[UPPER_RIGHT] == token ||
		squares[LOWER_LEFT] == token || squares[LOWER_RIGHT] == token
}

func getContinuousEdge(squares []uint8, opponent uint8) []uint8 {
	var firstFound uint8 = 11
	var largest []uint8
	var edge []uint8
	var i uint8
	looped := false
	for i = 0; !looped || i < firstFound; i++ {
		if i == firstFound {
			break
		}
		if squares[edges[i]] == opponent {
			if len(edge) >= len(largest) {
				largest = edge
				edge = []uint8{}
			}
			if firstFound == 11 {
				firstFound = i
			}
		} else {
			edge = append(edge, edges[i])
		}
		//Loop back around
		if int(i) == len(edges)-1 {
			i = 0
			looped = true
		}
	}
	if len(edge) >= len(largest) {
		largest = edge
	}
	return largest
}

func getBestEdge(squares []uint8, edge []uint8) int {
	for _, v := range edge {
		if v%2 == 0 {
			continue
		}
		if v == UPPER_CENTER && squares[LOWER_CENTER] == BLANK {
			return UPPER_CENTER
		} else if v == LOWER_CENTER && squares[UPPER_CENTER] == BLANK {
			return LOWER_CENTER
		} else if v == LEFT && squares[RIGHT] == BLANK {
			return LEFT
		} else if v == RIGHT && squares[LEFT] == BLANK {
			return RIGHT
		}
	}
	return -1
}

func getOpenCorner(squares []uint8) int {
	if squares[UPPER_LEFT] == BLANK {
		return UPPER_LEFT
	} else if squares[UPPER_RIGHT] == BLANK {
		return UPPER_RIGHT
	} else if squares[LOWER_LEFT] == BLANK {
		return LOWER_LEFT
	} else if squares[LOWER_RIGHT] == BLANK {
		return LOWER_RIGHT
	}
	return -1
}

func getMoveFive(squares []uint8) int {
	if squares[UPPER_CENTER] == X_INT && squares[CENTER] == O_INT {
		return LOWER_LEFT
	} else if squares[CENTER] == X_INT {
		return UPPER_RIGHT
	} else if squares[UPPER_RIGHT] == X_INT && squares[LOWER_RIGHT] == O_INT {
		return LOWER_LEFT
	}
	return CENTER
}

func getPossibleWin(squares []uint8, token uint8) int {
	for _, combo := range winningCombos {
		canWin := true
		move := -1
		for i := 0; i < 3; i++ {
			if squares[combo[i]] != BLANK && squares[combo[i]] != token {
				canWin = false
			} else if squares[combo[i]] == BLANK {
				move = int(combo[i])
			}
		}
		if canWin {
			return move
		}
	}
	return -1
}
